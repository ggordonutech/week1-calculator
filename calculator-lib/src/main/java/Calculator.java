/**
 * Calculator class
 * @author ggordon
 * @modified 11.2.2019
 * */
public class Calculator {
	
	/**
	 * Multiplies <i><strong>3</strong></i> numbers.
	 * @param num1 First number
	 * @param num2 Second number
	 * @param num3 Third number
	 * @return Product of three numbers
	 * */
	public float multiply(float num1, float num2, float num3) {
		return num1*num2*num3;
	}

}
